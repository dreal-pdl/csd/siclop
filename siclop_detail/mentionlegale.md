# Crédits - Mentions légales

Publié le 12 juin 2018

## Service gestionnaire

Direction Régionale de l’Environnement de l’Aménagement et du Logement des Pays de la Loire

5, rue Françoise Giroud

CS 16326

44263 NANTES Cedex 2

Tél : 02 72 74 73 00

Fax : 02 72 74 73 09

Courriel : dreal-paysdelaloire [at] developpement-durable.gouv.fr

## Directrice de publication

Anne Beauval, directrice régionale de l’environnement, de l’aménagement et du logement des Pays de la Loire.

## Conception, Réalisation

- Charte graphique, ergonomie : DREAL Pays de la Loire

- Développement : DREAL Pays de la Loire

## Hébergement

- Rstudio - plateforme Shinyapps - http://shinyapps.io/

## Droit d’auteur
Tous les contenus présents sur le site de la direction régionale de l’Environnement, de l’Aménagement et du Logement des Pays de la Loire sont couverts par le droit d’auteur. Toute reprise est dès lors conditionnée à l’accord de l’auteur en vertu de l’article L.122-4 du Code de la Propriété Intellectuelle.

## Établir un lien
- Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées par le Ministère de la Transition écologique et solidaire et le Ministère de la Cohésion des Territoires. 

- L’autorisation de mise en place d’un lien est valable pour tout support, à l’exception de ceux diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure porter atteinte à la sensibilité du plus grand nombre.

- Pour ce faire, et toujours dans le respect des droits de leurs auteurs, une icône "Marianne" est disponible [ici](http://www.pays-de-la-loire.developpement-durable.gouv.fr/local/cache-vignettes/L105xH136/arton1255-30208.png) pour agrémenter votre lien et préciser que le site d’origine est celui du Ministère de la Transition écologique et solidaire ou du Ministère de la Cohésion des Territoires.

## Usage

- Les utilisateurs sont responsables des interrogations qu’ils formulent ainsi que de l’interprétation et de l’utilisation qu’ils font des résultats. Il leur appartient d’en faire un usage conforme aux réglementations en vigueur et aux recommandations de la CNIL lorsque des données ont un caractère nominatif (loi n° 78.17 du 6 janvier 1978, relative à l’informatique, aux fichiers et aux libertés dite loi informatique et libertés). 

- Il appartient à l’utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par d’éventuels virus circulant sur le réseau Internet. De manière générale, la Direction Régionale de l’Environnement de l’Aménagement et du Logement des Pays de la Loire décline toute responsabilité à un éventuel dommage survenu pendant la consultation du présent site. Les messages que vous pouvez nous adresser transitant par un réseau ouvert de télécommunications, nous ne pouvons assurer leur confidentialité.
