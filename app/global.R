library(bsplus)
#library(COGiter)
library(dplyr)
library(DT)
library(formattable)
library(ggplot2)
library(highcharter)
library(htmltools)
library(jpeg)
library(kableExtra)
library(leaflet)
library(leaflet.extras)
library(lubridate)
#library(phantomjs)
#library(plotly)
library(png)
library(rAmCharts)
library(sf)
library(shiny)
library(shinyBS)
library(shinycustomloader)
library(shinydashboard)
library(shinydreal)
library(tidyverse)
library(viridis)
library(webshot)
library(htmlwidgets)
library(magick)
library(markdown)

jsfile <- "https://rawgit.com/rowanwins/leaflet-easyPrint/gh-pages/dist/bundle.js"


#modification des contours epci pour ne garder que la région
get_border_epci_reg <- function (code_region_insee) {
  reg_geo <- regions_geo %>% filter(REG == code_region_insee)
  epci_reg_geo <- epci_geo %>% inner_join(liste_zone %>% filter(TypeZone == 'Communes',str_detect(REG,code_region_insee)) %>% select(EPCI) %>% distinct()
  )
  return(st_intersection(epci_reg_geo, reg_geo))
} 