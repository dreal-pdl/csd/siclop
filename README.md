---
title: "Présentation de Siclop"
auteur: "Daniel Kalioudjoglou"
date: "20/06/2023"
output: html_document
---

## [![](app/www/SICLOP_blanc.png){width="114" height="43"}](http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/siclop/ "lien vers l'application")

Le système d'information et de collecte des loyers privés (SICLOP), développé par la DREAL Pays de la Loire, permet de connaître les loyers pratiqués sur la région Pays de la Loire. Il est consultable à l'adresse <http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/siclop/>.

Cet outil, développé par l'équipe du [« DREAL datalab »](http://www.pays-de-la-loire.developpement-durable.gouv.fr/dreal-datalab-centre-de-service-de-la-donnee-r1957.html "« DREAL datalab » (nouvelle fenetre)"), et mis en ligne en 2018, permet de disposer des niveaux de loyers de marché aux échelles régionale, départementale et intercommunale, croisés avec différentes variables (surfaces, typologies, modes de chauffage et performance énergétique).

## Objectifs de Siclop


|  |  |     
|--|--|     
| ![](app/www/illustration_siclop_rd.png){width="341"} | La connaissance du parc locatif privé est nécessaire à la conduite des politiques de l'habitat traduite dans les documents de planification tels que les programmes locaux de l'habitat (PLH), les plans départementaux de l'habitat (PDH) et les schémas de cohérence territoriale (SCoT). |   



## Précisions méthodologiques

SICLOP repose sur la collecte d'annonces immobilières de maisons et d'appartements en [location dans les Pays de la Loire](https://www.ouestfrance-immo.com/immobilier/location/logement/pays-de-la-loire/ "location dans les Pays de la Loire (nouvelle fenetre)") publiées sur le portail immobilier de Ouest-France Immobilier. Une méthodologie de recueil et de traitement des données, permettant d'obtenir des résultats fiables sur les loyers, a été développée par la DREAL.

Les collectes portent sur les loyers de marché et non sur les loyers de stock. Il est à noter que les loyers de marché sont généralement plus élevés que ceux du stock.

Les annonces sont collectées quotidiennement avec une mise à jour mensuelle du site internet. Elles permettent de rendre compte des flux sur le parc locatif privé et d'améliorer la connaissance du niveau et de l'évolution des loyers de marché sur l'ensemble des territoires de la région, et particulièrement ceux non couverts par un [observatoire local des loyers du réseau national](https://www.observatoires-des-loyers.org "observatoire local des loyers du réseau national (nouvelle fenetre)").

## Fonctionnement général du présent répertoire

L'application Siclop est une application RShiny développée en R par le Centre de Service de la Donnée (CSD) de la Dreal des Pays de la Loire. Une tache planifiée récupère quotidiennement les données des annonces sur le site de Ouest France Immobilier, leur applique un traitement de redressement des données, de mise en forme, puis de valorisation.

Les grandes étapes sont les suivantes:

-   Webscrapping : lancement automatique quotidien d'un script R qui récupère les annonces et leur contenu, sur le site de Ouest France Immobilier

-   Traitement des données : lancement de scripts R qui analysent et traitent les données, effectuent des redressements, suppriment les annonces non significatives...

-   Les données préparées sont placées dans l'application RShiny qui lorsque elle est lancée, réalise la valorisation des données (création de tableaux, graphiques et cartes)

### Web-scrapping et préparation des données

Les fichiers R correspondants sont disposés dans le dossier "scripts". Le lancement de l'opération s'effectue en lançant le fichier **0-script_maitre_routine_z800.R** si l'opération est lancée depuis l'appli placée sur le serveur T de la DREAL (opération automatique quotidienne), ou en lançant le fichier **0-script_maitre_serveur_dataviz.R**, si l'opération est lancée depuis le serveur dataviz de la Dreal. A l'éxecution d'un de ces fichiers, les opérations effectuées sont, [dans l'ordre]{.underline}, les suivantes:

-   lancement du script **OF-miseajour.R** :
      - chargement de l'ancienne base brute **OF.RData**
      - listage des nouvelles annonces (fonction disponible dans OF-fonctions.R)
      - lecture des annonces nouvelles et récupération des données (fonction disponible dans OF-fonctions.R)
      - enregistrement de la nouvelle base brute **OF.RData**
      - sauvegarde des données sur le serveur SGBD de la Dreal

-   lancement du script **OF-nettoyerannonces.R** : "nettoyage" des annonces

-   lancement du script **OF-datamart.R** : préparation des données (mise à un format facilement exploitable par l'application)

### dans le détail : nettoyage des annonces

Opérations effectuées à l'intérieur du fichier **OF-nettoyerannonces.R**

| Opération                         | Action                                                                                                                                         |  
|-----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|  
| filtre sur le montant des charges | montant supprimé si supérieur à 100 (il peut s'agir de l'état des lieux),  <br>  montant supprimé si inférieur à 10                                                                                                     |  
| récupération des données du titre | récupération de certaines données qui sont dans le titre de l'annonce (exemple : Appartement 4 pièces Nantes Zola) pour les placer dans le bon |  
| renommage                         | renommage des variables récupérées                                                                                                             |  
| ajout nom du quartier             | à partir du code quartier récupéré, nommage du quartier tel qu'intitulé sur le site d'Ouest France Immo                                        |   
| nettoyage variables numériques    | Nettoyer les variables numériques des unités (€, m2...)                                                                                        |   
| suppression annonces hors champ de l'enquête | Ne garder que les annonces locatives, suppression si : <br> - domaine différent de "location" <br> - transaction différent de "L" <br> - classification = 2722 (chambres) <br> - type hors champ : 218 (garage box et parking), 6205 (colocations), 6249 (résidence avec services), 213 (divers), 2726 (terrains), 222 (demande location) <br> - annonce dont description contient des mots en rapport avec la **défiscalisation** (anah, pinel, duflot, scellier, robien, borloo, convention, dispositif fiscal, réservé aux salariés, du secteur privé, des entreprises privées, conditions de ressources, défiscalisé) les **HLM** (logement social, pls, fichier commun des demandeurs, fichier des demandeurs, besson), et résidences (senior, âgées) <br>  - chambres meublées <br>  - annonce dont description contient formule faisant rapport avec l'accession à la propriété (alur, maison en accession, devenir propriétaire, devenez propriétaire, mikit, 1er achat, faire offre, accession sociale) <br>  - autre hors champ détectable via la description (local commercial, surface de vente, semaine, saisonnier(e), à la quinzaine, quinzaine dégressive)                                    |     
| suppression des annonces faibles surfaces | suppression si surface habitable manquante ou moins de 25 m2                                                                           |   
| analyse et récupération de données dans le champ description | Récupération dans le champ descriptif de l'annonce, des renseignements des champs standard (surface, type, loyers,étage, type de chauffage, ...) et remplacement dans les champs standard si ceux-ci ne sont pas renseignés. |   
| redressement de variables         | -   effacement du nombre de pièces si celui ci est supérieur à 15  <br> -   effacement honoraires d'agence si supérieurs à 5000 euros   <br> -   supprime les montants des charges supérieurs à 1000 euros  <br> -   effacement donnée étage si > 29    <br> -   effacement surfaces habitables < 9 m2 , entre 500 et 1000 m2 et > 15000 m2.      <br> -   division par 100 les valeurs entre 1000 et 15000 m2 (probable erreur de virgule)                                                            |    
| analyse des loyers                | comparaison des loyers avec ceux inscrits dans le commentaire de l'annonce. vérification de la valeur si inscrite avec ou sans charge (nombreuses formules de comparaison en fonction des termes utilisés) |    

### dans le détail : préparation des données

Opérations effectuées à l'intérieur du fichier **OF-datamart.R**

Ce script permet de préparer les données afin d'être facilement utilisée dans l'application shiny. Cette opération, comme la précédente, est effectuée avant la mise en ligne de l'application. Les opérations sont les suivantes:

-   passage au COG (code officiel géographique) à jour : Le code commune de chaque annonce est vérifié et modifié si besoin afin d'être conforme au dernier millésime connu

-   les données sont agrégées au niveau communal, EPCI, départemental et régional. Les tables créees sont prêtes à être utilisées par l'application

-   création des fichiers région et départements pour chauffage

-   création des listes de choix pour les menus déroulants

-   autres préparations diverses (préparation de données pour carte, calcul de variables)

-   enregistrement des différentes tables préparées

## L'application de consultation RShiny

**Phase développement**

L'application Siclop développée en RShiny est modifiée sur les postes des développeurs. Elle est versionnée sur [Gitlab-Forge](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/siclop).

**Versions en ligne**

Il existe 2 versions de Siclop en ligne, la version **Siclop** classique et la version **Siclop détail**

La version **Siclop** classique est diffusée à la fois sur internet et sur l'intranet ministère. La version **Siclop détail** est uniquement mise en ligne en intranet. 

Les versions intranet sont positionnées sur un serveur Rstudio hébergé à la DREAL Pays de la Loire.

La version internet est hébergée sur [shinyApps.io](https://www.shinyapps.io/ "lien vers l'offre shinyApps.io") grâce à l'offre commerciale souscrite par le Ministère de l'Ecologie. Le chargement des données sur ce serveur se fait manuellement à périodes régulières. L'application internet est rendue sous : <http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/siclop/>.
